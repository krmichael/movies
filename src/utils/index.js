import FontistoIcon from 'react-native-vector-icons/Fontisto';
import OctIcon from 'react-native-vector-icons/Octicons';
import IonIcon from 'react-native-vector-icons/Ionicons';
import FoundationIcon from 'react-native-vector-icons/Foundation';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';

const LINK_NAV = [
  { id: 1, label: 'Séries' },
  { id: 2, label: 'Filmes' },
  { id: 3, label: 'Minha lista' },
];

const SECTION_BUTTONS = [
  {
    id: 1,
    label: 'Minha Lista',
    icon: FontistoIcon,
    iconName: 'plus-a',
    size: 24,
    color: '#bbb',
    player: false,
  },
  {
    id: 2,
    label: 'Assistir',
    icon: FontistoIcon,
    iconName: 'play',
    size: 20,
    color: '#333',
    player: true,
  },
  {
    id: 3,
    label: 'Saiba mais',
    icon: IonIcon,
    iconName: 'md-information-circle-outline',
    size: 25,
    color: '#bbb',
    player: false,
  },
];

const LINK_FOOTER = [
  { id: 1, label: 'Início', icon: OctIcon, iconName: 'home' },
  { id: 2, label: 'Buscas', icon: OctIcon, iconName: 'search' },
  { id: 3, label: 'Em breve', icon: FoundationIcon, iconName: 'play-video' },
  { id: 4, label: 'Downloads', icon: FontistoIcon, iconName: 'download' },
  { id: 5, label: 'Mais', icon: MaterialIcon, iconName: 'menu' },
];

const ACTION_BUTTONS = [
  { id: 1, label: 'Minha lista', icon: AntDesignIcon, iconName: 'plus' },
  { id: 2, label: 'Classifique', icon: AntDesignIcon, iconName: 'like2' },
  { id: 3, label: 'Compartilhe', icon: AntDesignIcon, iconName: 'sharealt' },
  { id: 4, label: 'Baixar', icon: AntDesignIcon, iconName: 'download' },
];

export { LINK_NAV, SECTION_BUTTONS, LINK_FOOTER, ACTION_BUTTONS };
