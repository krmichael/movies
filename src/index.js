import React from 'react';
import { StatusBar } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { Provider } from 'react-redux';

import './config/ReactotronConfig';

import store from './store';
import Routes from './routes';

const theme = {
  ...DefaultTheme,
  dark: true,
  colors: {
    ...DefaultTheme.colors,
    primary: '#fff',
    background: '#1A191A',
    card: '#111',
    text: '#fff',
    border: '#fff',
  },
};

export default function App() {
  return (
    <Provider store={store}>
      <SafeAreaProvider>
        <StatusBar
          backgroundColor="transparent"
          barStyle="light-content"
          translucent={true}
        />
        <NavigationContainer theme={theme}>
          <Routes />
        </NavigationContainer>
      </SafeAreaProvider>
    </Provider>
  );
}
