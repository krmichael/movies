import React, { useEffect } from 'react';
import { ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import {
  movieDetailsRequest,
  similarMovieRequest,
} from '../../store/modules/details/actions';

import { ACTION_BUTTONS } from '../../utils';
import Gradient from '../../components/Gradient';
import SimilarMovies from '../../components/SimilarMovies';

import {
  Container,
  Banner,
  IconBanner,
  Image,
  Details,
  Title,
  Description,
  Relevant,
  Year,
  Age,
  Timer,
  Overview,
  MoviesCast,
  OverviewFooterTitle,
  OverviewFooterDescription,
  Direction,
  ScopeActions,
  ButtonAction,
  ButtonActionText,
} from './styles';

export default function DetailsComponent({ route }) {
  const { movieId } = route.params;

  const dispatch = useDispatch();
  //const loading = useSelector((state) => state.details.loading);
  const details = useSelector((state) => state.details.details);

  useEffect(() => {
    dispatch(movieDetailsRequest(movieId));
    dispatch(similarMovieRequest(movieId));
  }, [dispatch, movieId]);

  console.log(details);
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Container>
        <Gradient placement="top" />
        <Banner onPress={() => {}}>
          <IconBanner />
          <Image
            source={{
              uri: `https://image.tmdb.org/t/p/w300${details.backdrop_path}`,
            }}
          />
          <Gradient />
        </Banner>

        <Details>
          <Title>{details.original_title}</Title>

          <Description>
            <Relevant>95% relevante</Relevant>
            <Year>2020</Year>
            <Age>18</Age>
            <Timer>2h 16m</Timer>
          </Description>

          <Overview>{details.overview}</Overview>

          <MoviesCast>
            <OverviewFooterTitle>Elenco: </OverviewFooterTitle>
            <OverviewFooterDescription>
              Mark Wahlberg, Winston Duke, Alan Arkin
            </OverviewFooterDescription>
          </MoviesCast>

          <Direction>
            <OverviewFooterTitle>Direção: </OverviewFooterTitle>
            <OverviewFooterDescription>F. Gary Gray</OverviewFooterDescription>
          </Direction>
        </Details>

        <ScopeActions>
          {ACTION_BUTTONS.map((button) => (
            <ButtonAction key={button.id}>
              <button.icon name={button.iconName} size={25} color="#fff" />
              <ButtonActionText>{button.label}</ButtonActionText>
            </ButtonAction>
          ))}
        </ScopeActions>

        <SimilarMovies />
      </Container>
    </ScrollView>
  );
}
