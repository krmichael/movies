import styled from 'styled-components/native';
import { Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

const { width: MAX_WIDTH } = Dimensions.get('window');

export const Container = styled.View`
  flex: 1;
  position: relative;
`;

export const Banner = styled.TouchableOpacity`
  position: relative;
  align-items: center;
  justify-content: center;
  height: 250px;
`;

export const IconBanner = styled(Icon).attrs({
  name: 'playcircleo',
  size: 70,
  color: '#FFF',
})`
  position: relative;
  z-index: 3;
`;

export const Image = styled.Image.attrs({
  resizeMode: 'cover',
})`
  position: absolute;
  top: 0;
  left: 0;
  width: ${MAX_WIDTH}px;
  height: 250px;
`;

export const Details = styled.View`
  padding: 0 10px;
`;

export const Title = styled.Text`
  font-size: 24px;
  font-weight: bold;
  color: #fff;
`;

export const Description = styled.View`
  flex-direction: row;
  align-items: center;
  padding: 15px 0;
`;

export const Relevant = styled.Text`
  font-size: 18px;
  font-weight: bold;
  color: #60c277;
`;
export const Year = styled.Text`
  font-size: 12px;
  color: #aaa;
  margin: 0 10px;
`;

export const Age = styled.Text`
  background: orange;
  padding: 3px 5px;
  border-radius: 3px;
  color: #fff;
  font-size: 12px;
  font-weight: bold;
  margin-right: 10px;
`;
export const Timer = styled.Text`
  color: #aaa;
  font-size: 12px;
`;

export const Overview = styled.Text`
  color: #ccc;
  font-size: 16px;
  line-height: 22px;
  padding: 10px 0;
`;

export const MoviesCast = styled.Text`
  color: #ccc;
`;

export const OverviewFooterTitle = styled.Text`
  font-weight: bold;
`;

export const OverviewFooterDescription = styled.Text``;

export const Direction = styled.Text`
  color: #ccc;
`;

export const ScopeActions = styled.View`
  flex-direction: row;
  justify-content: space-around;
  padding: 20px 0;
`;

export const ButtonAction = styled.TouchableOpacity`
  max-width: 25%;
  margin: 0 20px;
  align-items: center;
  text-align: center;
`;

export const ButtonActionText = styled.Text`
  color: #fff;
  font-size: 12px;
  text-align: center;
  margin-top: 10px;
`;
