import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import {
  getGenresRequest,
  getMoviesRequest,
} from '../../store/modules/home/actions';

import Header from '../../components/Header';
import Banner from '../../components/Banner';
import ListMovies from '../../components/ListMovies';

import { Container, Scope } from './styles';

export default function Home() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getGenresRequest());

    dispatch(getMoviesRequest(28));
  }, [dispatch]);

  const genres = useSelector((state) => state.home.genres);
  const items = useSelector((state) => state.home.items);

  return (
    <Container>
      <Scope>
        <Header />
        <Banner />

        {genres.map((genre) => {
          const filtered = items.filter((item) =>
            item.genre.includes(genre.id),
          );

          return (
            <ListMovies
              key={genre.name}
              title={filtered.length > 0 && `Filmes de ${genre.name}`}
              data={filtered}
            />
          );
        })}
      </Scope>
    </Container>
  );
}
