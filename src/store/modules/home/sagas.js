import { all, takeLatest, call, put } from 'redux-saga/effects';
import { API_KEY } from 'react-native-dotenv';
import { Alert } from 'react-native';

import { api } from '../../../services';
import { getGenresSuccess, getMoviesSuccess } from './actions';

export function* getGenres() {
  try {
    const resp = yield call(
      api.get,
      `/genre/movie/list?api_key=${API_KEY}&language=ptBR`,
    );
    const data = resp.data.genres.map((genre) => ({
      id: genre.id,
      name: genre.name,
    }));

    yield put(getGenresSuccess(data));
  } catch (error) {
    Alert.alert('Error', 'erro ao carregar a lista de genres');
  }
}

export function* getMoviesByGenre({ payload }) {
  try {
    const { genre_id } = payload;

    const resp = yield call(
      api.get,
      `/discover/movie?api_key=${API_KEY}&page=1&sort_by=popularity.desc&with_genre=${genre_id}&language=ptBR`,
    );

    const items = resp.data.results.map((i) => ({
      id: i.id,
      image: i.poster_path,
      genre: i.genre_ids,
      title: i.title,
    }));

    yield put(getMoviesSuccess(items));
  } catch (error) {
    Alert.alert('Error', 'erro ao carregar a lista de movies');
  }
}

export default all([
  takeLatest('GET_GENRES_REQUEST', getGenres),
  takeLatest('GET_REQUEST', getMoviesByGenre),
]);
