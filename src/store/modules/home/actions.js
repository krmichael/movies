export function getGenresRequest() {
  return {
    type: 'GET_GENRES_REQUEST',
  };
}

export function getGenresSuccess(genres) {
  return {
    type: 'GET_GENRES_SUCCESS',
    payload: { genres },
  };
}

export function getMoviesRequest(genre_id) {
  return {
    type: 'GET_REQUEST',
    payload: { genre_id },
  };
}

export function getMoviesSuccess(items) {
  return {
    type: 'GET_SUCCESS',
    payload: { items },
  };
}
