import { all } from 'redux-saga/effects';

import home from './home/sagas';
import details from './details/sagas';

export default function* rootSaga() {
  return yield all([home, details]);
}
