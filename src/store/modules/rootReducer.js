import { combineReducers } from 'redux';

import home from './home/reducer';
import details from './details/reducer';

export default combineReducers({
  home,
  details,
});
