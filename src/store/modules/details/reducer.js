import { produce } from 'immer';

const INITIAL_STATE = {
  movieId: null,
  details: {},
  similars: [],
  loading: false,
};

export default function Details(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case 'MOVIE_DETAILS_REQUEST':
        draft.movieId = action.payload.movieId;
        draft.loading = true;
        break;

      case 'MOVIE_DETAILS_SUCCESS':
        draft.details = action.payload.details;
        draft.loading = false;
        break;

      case 'SIMILAR_MOVIE_REQUEST':
        draft.movieId = action.payload.movieId;
        draft.loading = true;
        break;

      case 'SIMILAR_MOVIES_SUCCESS':
        draft.similars = action.payload.similars;
        draft.loading = false;
        break;

      default:
        break;
    }
  });
}
