export function movieDetailsRequest(movieId) {
  return {
    type: 'MOVIE_DETAILS_REQUEST',
    payload: { movieId },
  };
}

export function movieDetailsSuccess(details) {
  return {
    type: 'MOVIE_DETAILS_SUCCESS',
    payload: { details },
  };
}

export function similarMovieRequest(movieId) {
  return {
    type: 'SIMILAR_MOVIE_REQUEST',
    payload: { movieId },
  };
}

export function similarMoviesSuccess(similars) {
  return {
    type: 'SIMILAR_MOVIES_SUCCESS',
    payload: { similars },
  };
}
