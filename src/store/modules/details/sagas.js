import { all, takeLatest, call, put } from 'redux-saga/effects';

import { movieDetailsSuccess, similarMoviesSuccess } from './actions';
import { API_KEY } from 'react-native-dotenv';
import { api } from '../../../services';

export function* detailsMovie({ payload }) {
  const { movieId } = payload;

  try {
    const response = yield call(
      api.get,
      `/movie/${movieId}?api_key=${API_KEY}&language=ptBR&append_to_response=videos`,
    );

    const details = response.data;

    yield put(movieDetailsSuccess(details));
  } catch (error) {
    console.error(error);
  }
}

export function* similarMovies({ payload }) {
  const { movieId } = payload;

  try {
    const response = yield call(
      api.get,
      `/movie/${movieId}/similar?api_key=${API_KEY}&page=1`,
    );

    const similars = response.data.results;

    yield put(similarMoviesSuccess(similars));
  } catch (error) {
    console.error(error);
  }
}

export default all([
  takeLatest('MOVIE_DETAILS_REQUEST', detailsMovie),
  takeLatest('SIMILAR_MOVIE_REQUEST', similarMovies),
]);
