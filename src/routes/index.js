import 'react-native-gesture-handler';

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Icon from 'react-native-vector-icons/AntDesign';
import IconM from 'react-native-vector-icons/MaterialIcons';

import HomeScreen from '../pages/Home';
import DetailsScreen from '../pages/Details';
import SearchScreen from '../pages/Search';
import CommingScreen from '../pages/Comming';
import DownloadsScreen from '../pages/Downloads';
import MoreScreen from '../pages/More';

const HomeStack = createStackNavigator();

const Tab = createBottomTabNavigator();

const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen
      name="Home"
      component={HomeScreen}
      options={{
        headerShown: false,
      }}
    />

    <HomeStack.Screen
      name="Details"
      component={DetailsScreen}
      options={{ headerTransparent: true, title: null }}
    />
  </HomeStack.Navigator>
);

export default function Routes() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      lazy={true}
      tabBarOptions={{
        background: 'red',
        inactiveTintColor: '#aaa',
        activeTintColor: '#fff',
        style: {
          borderTopColor: 0,
        },
        tabStyle: {
          backgroundColor: '#121212',
          elevation: 0,
          borderTopWidth: 0,
          borderTopColor: 'transparent',
          paddingTop: 5,
        },
        keyboardHidesTabBar: true,
      }}>
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          tabBarIcon: ({ focused, color }) => (
            <Icon name="home" size={20} focused={focused} color={color} />
          ),
          title: 'Início',
        }}
      />
      <Tab.Screen
        name="Buscar"
        component={SearchScreen}
        options={{
          tabBarIcon: ({ focused, color }) => (
            <Icon name="search1" size={20} focused={focused} color={color} />
          ),
          title: 'Buscas',
        }}
      />

      <Tab.Screen
        name="EmBreve"
        component={CommingScreen}
        options={{
          tabBarIcon: ({ focused, color }) => (
            <IconM name="airplay" size={20} focused={focused} color={color} />
          ),
          title: 'Em breve',
        }}
      />

      <Tab.Screen
        name="Downloads"
        component={DownloadsScreen}
        options={{
          tabBarIcon: ({ focused, color }) => (
            <Icon name="download" size={20} focused={focused} color={color} />
          ),
          title: 'Downloads',
        }}
      />

      <Tab.Screen
        name="Mais"
        component={MoreScreen}
        options={{
          tabBarIcon: ({ focused, color }) => (
            <IconM name="menu" size={20} focused={focused} color={color} />
          ),
          title: 'Mais',
        }}
      />
    </Tab.Navigator>
  );
}
