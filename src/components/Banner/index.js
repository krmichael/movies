import React from 'react';

import SectionButtons from '../SectionButtons';
import Gradient from '../Gradient';

import { Container, Image } from './styles';

import bgBanner from '../../assets/banner.jpg';

export default function Banner() {
  return (
    <Container>
      <Image source={bgBanner} />
      <SectionButtons />
      <Gradient />
    </Container>
  );
}
