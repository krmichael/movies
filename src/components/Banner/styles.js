import styled from 'styled-components/native';

export const Container = styled.View`
  position: relative;
`;

export const Image = styled.Image`
  width: 100%;
  height: 450px;
`;
