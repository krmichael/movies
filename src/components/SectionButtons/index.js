import React from 'react';

import { Container, Button, ButtonText } from './styles';
import { SECTION_BUTTONS } from '../../utils';

export default function SectionButtons() {
  return (
    <Container>
      {SECTION_BUTTONS.map((button) => (
        <Button play={button.player} key={button.id}>
          <button.icon
            name={button.iconName}
            size={button.size}
            color={button.color}
          />
          <ButtonText play={button.player}>{button.label}</ButtonText>
        </Button>
      ))}
    </Container>
  );
}
