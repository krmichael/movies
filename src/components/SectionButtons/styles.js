import styled, { css } from 'styled-components/native';

export const Container = styled.View`
  position: absolute;
  width: 100%;
  flex-direction: row;
  background-color: rgba(0, 0, 0, 0.1);
  justify-content: space-around;
  bottom: 0px;
  align-items: center;
  padding: 10px 5px 20px 5px;
  z-index: 4;
`;

export const Button = styled.TouchableOpacity.attrs((props) => ({
  play: props.play,
  activeOpacity: 0.7,
}))`
  align-items: center;
  ${(props) =>
    props.play &&
    css`
      flex-direction: row;
      background-color: #f2f2f2;
      padding: 10px 20px;
      border-radius: 3px;
    `}
`;

export const ButtonText = styled.Text.attrs((props) => ({
  play: props.play,
}))`
  color: #bbb;
  margin-top: 3px;

  ${(props) =>
    props.play &&
    css`
      color: #333;
      margin-left: 10px;
      margin-top: -3px;
      font-size: 20px;
      font-weight: bold;
    `}
`;
