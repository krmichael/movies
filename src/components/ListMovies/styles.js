import styled from 'styled-components/native';

export const Container = styled.View`
  margin-bottom: 10px;
`;

export const Title = styled.Text`
  color: #fff;
  font-size: 16px;
  font-weight: bold;
  padding: 0 10px;
`;

export const List = styled.FlatList`
  padding: 0 4px;
`;

export const ListItem = styled.TouchableOpacity`
  margin: 0 6px;
`;

export const Image = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: 105px;
  height: 180px;
`;
