import React from 'react';
import { useNavigation } from '@react-navigation/native';

import { Container, List, ListItem, Image, Title } from './styles';

export default function ListMovies({ title, data }) {
  const navigation = useNavigation();

  return (
    <Container>
      {title && <Title>{title}</Title>}

      <List
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        data={data}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <ListItem
            onPress={() =>
              navigation.navigate('Details', { movieId: item.id })
            }>
            <Image
              source={{
                uri: `https://image.tmdb.org/t/p/w154${item.image}`,
              }}
            />
          </ListItem>
        )}
      />
    </Container>
  );
}
