import React from 'react';
import { useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import {
  Container,
  Description,
  DescriptionText,
  List,
  ListItem,
  ListImage,
} from './styles';

export default function SimilarMovies() {
  const similars = useSelector((state) => state.details.similars);
  const navigation = useNavigation();

  return (
    <Container>
      <Description>
        <DescriptionText>Opções semelhantes</DescriptionText>
      </Description>

      <List>
        {similars?.map((movie) => (
          <ListItem
            key={movie.id}
            onPress={() => navigation.push('Details', { movieId: movie.id })}>
            <ListImage
              source={{
                uri: `https://image.tmdb.org/t/p/w154${movie.poster_path}`,
              }}
            />
          </ListItem>
        ))}
      </List>
    </Container>
  );
}
