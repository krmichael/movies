import styled from 'styled-components/native';
import { Dimensions } from 'react-native';

const { width: MAX_WIDTH } = Dimensions.get('window');

export const Container = styled.View`
  flex: 1;
  border-top-width: 4px;
  border-style: solid;
  border-top-color: #111;
  margin: 10px 0px;
`;

export const Description = styled.View`
  height: 50px;
  border-top-width: 5px;
  border-style: solid;
  border-top-color: red;
  margin-left: 10px;
  width: 50%;
`;

export const DescriptionText = styled.Text`
  font-size: 16px;
  color: #fff;
  font-weight: bold;
  text-transform: uppercase;
  margin: 15px 0px;
`;

export const List = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: 10px;
`;

export const ListItem = styled.TouchableOpacity`
  width: ${MAX_WIDTH / 3 - 4.7}px;
  height: auto;
  border: 4px solid transparent;
`;

export const ListImage = styled.Image`
  width: 100%;
  height: 170px;
`;
