import React from 'react';
import FontistoIcon from 'react-native-vector-icons/Fontisto';
import { useSafeArea } from 'react-native-safe-area-context';

import Gradient from '../Gradient';
import { Container, Link } from './styles';
import { LINK_NAV } from '../../utils';

export default function Header() {
  const insets = useSafeArea();

  return (
    <>
      <Gradient placement="top" />
      <Container style={{ paddingTop: insets.top }}>
        <FontistoIcon name="netflix" size={40} color="red" />

        {LINK_NAV.map((link) => (
          <Link key={link.id}>{link.label}</Link>
        ))}
      </Container>
    </>
  );
}
