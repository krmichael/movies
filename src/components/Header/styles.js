import styled from 'styled-components/native';

export const Container = styled.View`
  position: absolute;
  width: 100%;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  margin-top: 10px;
  margin-bottom: 10px;
  z-index: 2;
`;

export const Link = styled.Text`
  color: #fff;
  font-size: 18px;
  letter-spacing: 1px;
`;
