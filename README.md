> ### Movies App

<figure>
  <video controls autoplay loop allowfullscreen="true" poster="src/assets/banner.jpg">
    <source src="https://gitlab.com/krmichael/movies/-/blob/master/src/assets/movieApp.mp4" type="video/mp4" />
  </video>
</figure>
